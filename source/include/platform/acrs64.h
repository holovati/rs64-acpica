#ifndef __ACRS64_H__
#define __ACRS64_H__

/* #ifdef __KERNEL__ */

/*ACPICA external files should not include ACPICA headers directly. */

/* #if !defined(BUILDING_ACPICA) && !defined(_RS64_ACPI_H) */
/* #error "Please don't include <acpi/acpi.h> directly, include <rs64/acpi.h>
 * instead." */
/* #endif */

/*#endif*/

/* Common (in-kernel/user-space) ACPICA configuration */

//#define ACPI_USE_SYSTEM_CLIBRARY
#define ACPI_USE_DO_WHILE_0
#define ACPI_USE_LOCAL_CACHE

//#define ACPI_IGNORE_PACKAGE_RESOLUTION_ERRORS

#define ACPI_MUTEX_TYPE ACPI_OSL_MUTEX

#ifdef __KERNEL__

//#define ACPI_USE_SYSTEM_INTTYPES
#  define ACPI_USE_GPE_POLLING

/* Kernel specific ACPICA configuration */

#  ifdef CONFIG_ACPI_REDUCED_HARDWARE_ONLY
#    define ACPI_REDUCED_HARDWARE 1
#  endif

#  ifdef CONFIG_ACPI_DEBUGGER
#    define ACPI_DEBUGGER
#  endif

#  ifdef CONFIG_ACPI_DEBUG
#    define ACPI_MUTEX_DEBUG
#  endif

//#include <string.h>
//#include <kernel.h>
//#include <ctype.h>

#  define CPU_L1_SIZE 64

#  define CPU_DATA_ALIGN 16

#  define WORD_SIZE 64

/* #include <linux/sched.h> */
/* #include <linux/atomic.h> */
/* #include <linux/math64.h> */
/* #include <linux/slab.h> */
/* #include <linux/spinlock_types.h> */
/* #ifdef EXPORT_ACPI_INTERFACES */
/* #include <linux/export.h> */
/* #endif */
/* #ifdef CONFIG_ACPI */
/* #include <asm/acenv.h> */
/* #endif */

//#define ACPI_INIT_FUNCTION __init

#  ifndef CONFIG_ACPI

/* External globals for __KERNEL__, stubs is needed */

//#define ACPI_GLOBAL(t,a)
//#define ACPI_INIT_GLOBAL(t,a,b)
/* Generating stubs for configurable ACPICA macros */

//#define ACPI_NO_MEM_ALLOCATIONS

/* Generating stubs for configurable ACPICA functions */

//#define ACPI_NO_ERROR_MESSAGES
#    define ACPI_DEBUG_OUTPUT
#  endif /* CONFIG_ACPI */

/* Host-dependent types and defines for in-kernel ACPICA */
#  define ACPI_MACHINE_WIDTH WORD_SIZE

#  define ACPI_USE_NATIVE_MATH64
#  define ACPI_EXPORT_SYMBOL(symbol) extern typeof(symbol) symbol;
#  define strtoul simple_strtoul

/* Based on FreeBSD's due to lack of documentation */
extern int AcpiOsAcquireGlobalLock(uint32_t *lock);
extern int AcpiOsReleaseGlobalLock(uint32_t *lock);

#  define ACPI_ACQUIRE_GLOBAL_LOCK(GLptr, Acq)                                 \
    do {                                                                       \
      (Acq) = AcpiOsAcquireGlobalLock(&((GLptr)->GlobalLock));                 \
    } while (0)

#  define ACPI_RELEASE_GLOBAL_LOCK(GLptr, Acq)                                 \
    do {                                                                       \
      (Acq) = AcpiOsReleaseGlobalLock(&((GLptr)->GlobalLock));                 \
    } while (0)

#  define ACPI_CACHE_T ACPI_MEMORY_LIST

#  define ACPI_SPINLOCK void *
#  define ACPI_CPU_FLAGS unsigned long

#  define USE_NATIVE_ALLOCATE_ZEROED

#endif /* __KERNEL__ */

#endif /* __ACRS64_H__ */
